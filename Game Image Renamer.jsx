// Renames and converts files as needed for games on the WW desktop and React flow.
#target photoshop

// Open folder selection dialog (for user to chose folder):
alert("You will be prompted for the folder containing your images.");
var folder = Folder.selectDialog();
if (!folder) {alert("Cancelled"); exit;}

// Open folder selection dialog (for user to chose target folder):
alert("You will be prompted for the folder where the new images will be saved.\n" +
      "It is recommended that this folder be source-controlled so it can be most " + 
      "easily iterated - this will overwrite existing images.");
var target_folder = Folder.selectDialog();
if (!target_folder) {alert("Cancelled"); exit;}

// Now open dialogues to get the needed info.
// This list can be extended or contracted as needed to support the project's needed variables.
// In this example, the incoming filenames all share a common prefix, for example, "SR",
// that will change each time new source images are provided for a new project.
var filename_prefix = prompt("Enter the common filename prefix here.", ""); 

// In this example, the new images created by the script will sometimes need to be named with
// an internal machine name representing the asset being referenced, and sometimes will need
// to be named with an integer ID representing the asset. This will set up variables for both.
var machine_name = prompt("Type in the internal machine name for the asset.", ""); 
    machine_name = machine_name.toLowerCase();
var internal_id = prompt("Enter the internal ID for the asset.", "");

// in this example, most images will come in as PNG and a few will need to be converted to JPG
// or GIF. These variables are used to have common save-as options applied to those conversions.
var jpegOptions = new JPEGSaveOptions();
jpegOptions.quality = 8;
jpegOptions.matte = MatteType.WHITE;

var gifOptions = new GIFSaveOptions();
gifOptions.transparency = true;

// set up a before and after set of data so we can translate.
// a couple generic exmples are included here to see what those might look like.
var image_conversion_matrix = [
	{
		// This name is not currently used but can help humans reading the script
		// to identify the image more easily.
		name: "120px Square Image",
		// This is the origin filename, which gets the origin path appended to it later.
		// It uses the common "filename_prefix" that is set per-project.
		source_file_name: filename_prefix + "_120x120.png",
		// This flags whether a conversion is in the offing later.
		conversion_needed: true,
		// When conversion_needed === true, this will be the type of conversion.
		// This could also be pulled out of the target file name later if desired, but
		// this is more explicit.
		conversion: "jpg",
		// This is later combined with the main target_folder input by the user to place the image
		// in its final location upon save.
		target_subfolder: "test/path/etcetera/",
		// This is the final file name when the image is saved to its final location.
		// Examples are shown here using the machine_name and later, the internal_id
		// used by this particular project in various places.
		file_name: "120_"+ machine_name + ".jpg"
	},
	{
		name: "More Info Modal Background",
		source_file_name: filename_prefix + "_WebHeader_954x328.png",
		conversion_needed: false, // note that there is no conversion k/v here as no conversion is needed.
		target_subfolder: "test/path/etcetera/modal/",
		file_name: internal_id + ".png"
	},
	// This is a very simplified example with only two images - few enough that manual conversion would not 
	// be time-prohibitive. In the reality of this project, the JSON object contains roughly twenty images.
];

// ScriptSettings is built from the tool at https://scriptui.joonas.me/ to create a user-facing dialog
// showing all the data input along the way. It's mostly good for debugging at the moment, so users of the script
// might want to remove this section or comment out the ScriptSettings.show() method call to skip this step.
var ScriptSettings = new Window("dialog"); 
    ScriptSettings.text = "Script Settings"; 
    ScriptSettings.orientation = "column"; 
    ScriptSettings.alignChildren = ["center","top"]; 
    ScriptSettings.spacing = 10; 
    ScriptSettings.margins = 16; 

var statictext1 = ScriptSettings.add("group"); 
    statictext1.orientation = "column"; 
    statictext1.alignChildren = ["left","center"]; 
    statictext1.spacing = 0; 

    statictext1.add("statictext", undefined, "Script executing with the following properties:", {name: "statictext1"}); 
    statictext1.add("statictext", undefined, "", {name: "statictext1"}); 
    statictext1.add("statictext", undefined, "Input Folder: " + folder.toString(), {name: "statictext1"}); 
    statictext1.add("statictext", undefined, "Output Base Folder: " + target_folder.toString(), {name: "statictext1"}); 
    statictext1.add("statictext", undefined, "Input File Prefix: " + filename_prefix, {name: "statictext1"}); 
    statictext1.add("statictext", undefined, "OCX Name: " + ocxname, {name: "statictext1"}); 
    statictext1.add("statictext", undefined, "Game ID:  " + game_id, {name: "statictext1"}); 
    statictext1.add("statictext", undefined, "", {name: "statictext1"}); 

var ok = ScriptSettings.add("button", undefined, undefined, {name: "ok"}); 
    ok.text = "Continue";     

ScriptSettings.show();

// For each image: open, then do what needs to be done:
for (var i = 0; i < image_conversion_matrix.length; i++) {
  // Select the i-th file in the object created above for further ops.
  var file = image_conversion_matrix[i];
  // Open the image.
  var img = app.open(File(folder + "/" + file.source_file_name));
  // Set the final target for the image based on the values set in the object.
  var saveFilePath = File(target_folder + "/" + file.target_subfolder + "/" + file.file_name);
  // Fork between whether this is going to be a converted file or a straight "copy."
  if (file.conversion_needed) {
 	   if (file.conversion === "jpg") {
 	   	// If you try to switch a PNG to a JPG, it might often think it has layers. 
 	   	// This will protect against errors when the JPG doesn't understand what's happening.
 	   	img.flatten();
 	   	// This handles cases when the input is an indexed-color PNG and protects against errors
 	   	// caused by Photoshop not understanding what to do with an invalid color model for JPG.
 	   	img.changeMode(ChangeMode.RGB);
	  	img.saveAs(saveFilePath, jpegOptions, false, Extension.LOWERCASE);
	  } else if (file.conversion === "gif") {
	  	img.saveAs(new File(saveFilePath), gifOptions, false, Extension.LOWERCASE);
	  }
  } else {
  	// This block handles images that don't *really* need to change. There's no working file-copy
  	// mechanism available to Photoshop that I know of in this object model, so this simply saves a new copy
  	// of the original file with the correct file name in the correct final location.
  	if (file.source_file_name.indexOf("gif") > -1 ) {
		img.saveAs(new File(saveFilePath), gifOptions, false, Extension.LOWERCASE);
  	} else {
 		img.saveAs(new File(saveFilePath), new PNGSaveOptions(), false, Extension.LOWERCASE); 		
  	}
  }
  // Leave the original image unchanged to avoid being destructive.
  img.close(SaveOptions.DONOTSAVECHANGES);
};
